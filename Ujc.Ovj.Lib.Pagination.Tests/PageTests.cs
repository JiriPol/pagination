﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Ujc.Ovj.Lib.Pagination.Abstraction;


namespace Ujc.Ovj.Lib.Pagination.Tests
{
    [TestClass()]
    public class PageTests
    {

        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void PageIntMinTest()
        {
            IPage page = Page.Factory.Create(int.MinValue);
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void PageNegativeOneTest()
        {
            IPage page = Page.Factory.Create(-1);
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void PageZeroTest()
        {
            IPage page = Page.Factory.Create(0);
        }

        [TestMethod()]
        public void PageOneTest()
        {
            IPage page = Page.Factory.Create(1);
            Assert.IsTrue(page.IsFirst);
            Assert.IsFalse(page.IsLast);
            Assert.AreEqual(1, page.SequentialNumber);
        }

        [TestMethod()]
        public void PageIntMaxTest()
        {
            IPage page = Page.Factory.Create(int.MaxValue);
            Assert.IsTrue(page.IsLast);
            Assert.IsFalse(page.IsFirst);
            Assert.AreEqual(int.MaxValue, page.SequentialNumber);
        }

        [TestMethod()]
        public void NextOneTest()
        {
            IPage page = Page.Factory.Create(1);
            Page nextPage = page.Next() as Page;
            Assert.IsNotNull(nextPage);
            Assert.AreEqual(2, nextPage.SequentialNumber);
            Assert.IsFalse(nextPage.IsFirst);
            Assert.IsFalse(nextPage.IsLast);
        }

        [TestMethod()]
        [ExpectedException(typeof(OverflowException))]
        public void NextIntMaxTest()
        {
            IPage page = Page.Factory.Create(int.MaxValue);
            Page nextPage = page.Next() as Page;           
        }

        [TestMethod()]
        [ExpectedException(typeof(OverflowException))]
        public void PreviousOneTest()
        {
            IPage page = Page.Factory.Create(1);
            Page nextPage = page.Previous() as Page;
        }

        [TestMethod()]
        public void PreviousTwoTest()
        {
            IPage page = Page.Factory.Create(2);
            Page previousPage = page.Previous() as Page;
            Assert.IsNotNull(previousPage);
            Assert.AreEqual(1, previousPage.SequentialNumber);
            Assert.IsTrue(previousPage.IsFirst);
            Assert.IsFalse(previousPage.IsLast);
        }
    }
}