﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Ujc.Ovj.Lib.Pagination.Abstraction;

namespace Ujc.Ovj.Lib.Pagination.Tests
{
    [TestClass()]
    public class FolioFormatterTests
    {
        [TestMethod()]
        public void GetFormatTest()
        {
            IPage page = Page.Factory.CreateFirst();
            string result1 = string.Format(new QuartoFormatter(), "{0:BOOK_LETTER}{0:NUMBER_ARABIC}{0:SIDE_RECTO_VERSO}", page);
            IPage nextPage = page.Next();
            string result2 = string.Format(new QuartoFormatter(), "{0:BOOK_LETTER}{0:NUMBER_ARABIC}{0:SIDE_RECTO_VERSO}", nextPage);
            Console.WriteLine(result1);
            Console.WriteLine(result2);

        }

        [TestMethod()]
        public void FormatTest()
        {
            Assert.Fail();
        }
    }
}