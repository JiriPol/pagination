﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Ujc.Ovj.Lib.Pagination.Abstraction;

namespace Ujc.Ovj.Lib.Pagination.Tests
{
    [TestClass()]
    public class QuartoFormatterTests
    {
        [TestMethod()]
        public void GetFormatTest()
        {
            IPage page = Page.Factory.CreateFirst();
            while (page.SequentialNumber < 600)
            {
                string format = string.Format(new QuartoFormatter(), "{0:BOOK_LETTER}{0:NUMBER_ARABIC}{0:SIDE_RECTO_VERSO}", page);
                Console.WriteLine(format);

                page = page.Next();
            }
        }

        [TestMethod()]
        public void FormatTest()
        {
            Assert.Fail();
        }
    }
}