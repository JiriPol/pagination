﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ujc.Ovj.Lib.Pagination.SideFormatters
{
    internal class AbSideFormatter : IFormatProvider, ICustomFormatter
    {
        public object GetFormat(Type formatType)
        {
            if (formatType == typeof(ICustomFormatter))
            {
                return this;
            }

            return null;
        }

        public string Format(string format, object arg, IFormatProvider formatProvider)
        {
            if (!(arg is int))
            {
                throw new ArgumentException();
            }

            int sideNumber = (int)arg;
            if (sideNumber < 0 || sideNumber > 1)
            {
                throw new ArgumentException("Value must be zero or one.");
            }

            if (sideNumber == 0)
            {
                return "a";
            }
            else
            {
                return "b";
            }
        }
    }
}
