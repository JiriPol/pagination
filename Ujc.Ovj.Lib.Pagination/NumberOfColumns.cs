﻿using System;
using Ujc.Ovj.Lib.Pagination.Abstraction;

namespace Ujc.Ovj.Lib.Pagination
{
    public struct NumberOfColumns : INumberOfColumns
    {
        private readonly int m_numberOfColumns;

        private NumberOfColumns(int numberOfColumns)
        {
            if (numberOfColumns < 2)
            {
                throw new ArgumentOutOfRangeException(nameof(numberOfColumns), $"{nameof(numberOfColumns)} must be greater than 1.");
            }

            m_numberOfColumns = numberOfColumns;
        }

        public int Value => m_numberOfColumns;

        public static class Factory
        {
            public static NumberOfColumns Create(int numberOfColumns)
            {
                return new NumberOfColumns(numberOfColumns);
            }
        }
    }
}
