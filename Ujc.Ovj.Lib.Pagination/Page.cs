﻿using System;
using Ujc.Ovj.Lib.Pagination.Abstraction;

namespace Ujc.Ovj.Lib.Pagination
{
    public class Page : IPage
    {
        private readonly int m_sequentialNumber;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="sequentialNumber">Sequential number of page. Starts with 1.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if <see cref="sequentialNumber"/> is less than 1.</exception>
        private Page(int sequentialNumber)
        {
            if (sequentialNumber < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(sequentialNumber), $"Parameter can only be a positive integer.");
            }

            m_sequentialNumber = sequentialNumber;
        }

        public bool IsFirst => m_sequentialNumber == 1;
        public bool IsLast => m_sequentialNumber == int.MaxValue;
        public int SequentialNumber => m_sequentialNumber;

        /// <summary>
        /// Returns next page.
        /// </summary>
        /// <returns>Next page</returns>
        /// <exception cref="OverflowException">Thrown if <see cref="SequentialNumber"/> is equal to <see cref="int.MaxValue"/></exception>
        public IPage Next()
        {
            if (IsLast)
            {
                throw new OverflowException($"Sequential integer overflow. Sequential number is integer max value ({m_sequentialNumber})");
            }

            return new Page(m_sequentialNumber + 1);
        }

        /// <summary>
        /// Returns previous page.
        /// </summary>
        /// <returns>Previous page</returns>
        /// <exception cref="OverflowException">Thrown if <see cref="SequentialNumber"/> is equal to 1</exception>
        public IPage Previous()
        {
            if (IsFirst)
            {
                throw new OverflowException("Sequential integer is 1 and cannot be less than 1.");
            }

            return new Page(m_sequentialNumber - 1);
        }

        public override string ToString()
        {
            return m_sequentialNumber.ToString();
        }

        public static class Factory
        {
            public static IPage Create(int sequentialNumber)
            {
                return new Page(sequentialNumber);
            }

            public static IPage CreateFirst()
            {
                return new Page(1);
            }
        }
    }
}
