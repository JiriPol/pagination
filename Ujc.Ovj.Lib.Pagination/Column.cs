﻿using System;
using Ujc.Ovj.Lib.Pagination.Abstraction;
using Ujc.Ovj.Lib.Pagination.Util;

namespace Ujc.Ovj.Lib.Pagination
{
    public class Column : IColumn
    {
        private readonly int m_value;
        private readonly int m_modulus;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="value">Numerical value of column.</param>
        /// <param name="numberOfColumns">Max. number of columns.</param>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Thrown if <see cref="value"/> is less than 0 or <see cref="numberOfColumns"/> is not greater than <see cref="value"/>
        /// </exception>
        private Column(int value, INumberOfColumns numberOfColumns)
        {
            if (value < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(value), $"{nameof(value)} must be greater or equal 1");
            }
            if (!(value < numberOfColumns.Value))
            {
                throw new ArgumentOutOfRangeException(nameof(value), $"{nameof(value)} must be less than {nameof(numberOfColumns)}");
            }

            m_value = value;
            m_modulus = numberOfColumns.Value;
        }

        public int Value => m_value;
        public bool IsFirst => m_value == 1;
        public bool IsLast => m_value == m_modulus;
        public INumberOfColumns NumberOfColumns => Pagination.NumberOfColumns.Factory.Create(m_modulus);
        
        /// <summary>
        /// Returns next column.
        /// </summary>
        /// <returns>Next column</returns>
        public IColumn Next()
        {
            int nextValue = checked(Value + 1);
            int nextValueMod = nextValue.Mod(m_modulus);
            if (nextValueMod == 0)
            {
                nextValueMod = m_modulus;
            }

            return new Column(nextValueMod, NumberOfColumns);
        }

        /// <summary>
        /// Returns previous column.
        /// </summary>
        /// <returns>Previous column</returns>
        public IColumn Previous()
        {
            int previousValue = checked(Value - 1);
            int previousValueMod = previousValue.Mod(m_modulus);
            if (previousValueMod == 0)
            {
                previousValueMod = m_modulus;
            }

            return new Column(previousValueMod, NumberOfColumns);
        }

        public static class Factory
        {
            public static IColumn Create(int value, INumberOfColumns numberOfColumns)
            {
                return new Column(value, numberOfColumns);
            }

            public static IColumn CreateFirst(INumberOfColumns numberOfColumns)
            {
                return new Column(1, numberOfColumns);
            }
        }
    }
}
