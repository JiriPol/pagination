﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ujc.Ovj.Lib.Pagination.Util
{
    internal static class IntExtensions
    {
        internal static int Mod(this int a, int n)
        {
            return ((a % n) + n) % n;
        }
    }
}
