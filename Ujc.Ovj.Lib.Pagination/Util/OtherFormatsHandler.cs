﻿using System;
using System.Globalization;

namespace Ujc.Ovj.Lib.Pagination.Util
{
    internal static class OtherFormatsHandler
    {
        internal static string HandleOtherFormats(string format, object arg)
        {
            if (arg is IFormattable)
            {
                return ((IFormattable)arg).ToString(format, CultureInfo.CurrentCulture);
            }
            else if (arg != null)
            {
                return arg.ToString();
            }
            else
            {
                return string.Empty;
            }
        }
    }
}
