﻿using System;
using Ujc.Ovj.Lib.Pagination.Abstraction;
using Ujc.Ovj.Lib.Pagination.BookFormatFormatters;
using Ujc.Ovj.Lib.Pagination.ColumnFormatters;
using Ujc.Ovj.Lib.Pagination.NumberFormatters;
using Ujc.Ovj.Lib.Pagination.SideFormatters;
using Ujc.Ovj.Lib.Pagination.Util;

namespace Ujc.Ovj.Lib.Pagination
{
    /// <summary>
    /// Compatible formats:
    /// BOOK_LETTER
    /// NUMBER_ARABIC
    /// NUMBER_ROMAN
    /// SIDE_RECTO_VERSO
    /// SIDE_A_B
    /// </summary>
    public class Sextodecimo : IFormatProvider, ICustomFormatter
    {
        private const int NUMBER_MODULO = 16;
        private const int NUMBER_OF_PAGES = 32;

        private const string GENERAL = "G";

        private const string BOOK_LETTER = "BOOK_LETTER";
        private const string NUMBER_ARABIC = "NUMBER_ARABIC";
        private const string NUMBER_ROMAN = "NUMBER_ROMAN";
        private const string SIDE_RECTO_VERSO = "SIDE_RECTO_VERSO";
        private const string SIDE_A_B = "SIDE_A_B";

        public object GetFormat(Type formatType)
        {
            if (formatType == typeof(ICustomFormatter))
            {
                return this;
            }

            return null;
        }

        public string Format(string format, object arg, IFormatProvider formatProvider)
        {
            if ((arg is IPage))
            {
                IPage page = (IPage)arg;
                bool isEven = page.SequentialNumber % 2 == 0;
                int number;
                int side;

                if (isEven)
                {
                    number = (page.SequentialNumber / 2) % NUMBER_MODULO;
                    side = 1;
                }
                else
                {
                    number = ((page.SequentialNumber + 1) / 2) % NUMBER_MODULO;
                    side = 0;
                }


                int bookFormat = (int)Math.Floor((decimal)page.SequentialNumber / NUMBER_OF_PAGES);
                if (page.SequentialNumber % NUMBER_OF_PAGES == 0)
                {
                    bookFormat--;
                }


                if (number == 0)
                {
                    number = NUMBER_MODULO;
                }

                if (format == null)
                {
                    format = GENERAL;
                }

                switch (format.ToUpperInvariant())
                {
                    case GENERAL:
                        return $"{this.GetType().Name}[{page.ToString()}]";
                    case BOOK_LETTER:
                        return string.Format(new LetterBookFormatFormatter(), "{0}", bookFormat);
                    case NUMBER_ARABIC:
                        return string.Format(new ArabicNumberFormatter(), "{0}", number);
                    case NUMBER_ROMAN:
                        return string.Format(new RomanNumberFormatter(), "{0}", number);
                    case SIDE_RECTO_VERSO:
                        return string.Format(new RectoVersoSideFormatter(), "{0}", side);
                    case SIDE_A_B:
                        return string.Format(new AbSideFormatter(), "{0}", side);
                    default:
                        throw new FormatException($"The format of '{format}' is invalid.");
                }
            }

            if (arg is IColumn)
            {
                return new ColumnFormatter().Format(format, arg, formatProvider);
            }

            return OtherFormatsHandler.HandleOtherFormats(format, arg);
        }
    }
}
