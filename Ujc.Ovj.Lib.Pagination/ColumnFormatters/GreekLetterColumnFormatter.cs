﻿using System;

namespace Ujc.Ovj.Lib.Pagination.ColumnFormatters
{
    internal class GreekLetterColumnFormatter : IFormatProvider, ICustomFormatter
    {
        private static readonly char[] Letters = new[] {
            'α', 'β', 'γ', 'δ', 'ε', 'ζ', 'η', 'θ',
            'ι', 'κ', 'λ', 'μ', 'ν', 'ξ', 'ο', 'π',
            'ρ', 'σ', 'τ', 'υ', 'φ', 'χ', 'ψ', 'ω'};

        public object GetFormat(Type formatType)
        {
            if (formatType == typeof(ICustomFormatter))
            {
                return this;
            }

            return null;
        }

        public string Format(string format, object arg, IFormatProvider formatProvider)
        {
            if (!(arg is int))
            {
                throw new ArgumentException();
            }

            int sideNumber = (int)arg;
            if (sideNumber < 0 || sideNumber >= Letters.Length)
            {
                throw new ArgumentException($"Value must be in the range 0 - {Letters.Length}.");
            }

            return Letters[sideNumber].ToString();
        }
    }
}
