﻿using System;
using Ujc.Ovj.Lib.Pagination.Abstraction;
using Ujc.Ovj.Lib.Pagination.NumberFormatters;
using Ujc.Ovj.Lib.Pagination.Util;

namespace Ujc.Ovj.Lib.Pagination.ColumnFormatters
{
    internal class ColumnFormatter : ICustomFormatter
    {
        public string Format(string format, object arg, IFormatProvider formatProvider)
        {
            if (arg is IColumn)
            {
                IColumn column = (IColumn)arg;
                int value = column.Value;
                switch (format)
                {
                    case "COLUMN_LATIN_LETTER":
                        return string.Format(new LatinLetterColumnFormatter(), "{0}", value);
                    case "COLUMN_GREEK_LETTER":
                        return string.Format(new GreekLetterColumnFormatter(), "{0}", value);
                    case "COLUMN_ARABIC_NUMBER":
                        return string.Format(new ArabicNumberFormatter(), "{0}", value);
                    case "COLUMN_ROMAN_NUMBER":
                        return string.Format(new RomanNumberFormatter(), "{0}", value);
                    default:
                        throw new FormatException($"The format of '{format}' is invalid.");
                }

            }

            return OtherFormatsHandler.HandleOtherFormats(format, arg);
        }
    }
}
