﻿using System;
using System.Text;

namespace Ujc.Ovj.Lib.Pagination.NumberFormatters
{
    internal class RomanNumberFormatter : IFormatProvider, ICustomFormatter
    {
        private static readonly int[] Values = new[] { 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 };
        private static readonly string[] Numerals = new[] { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" };

        public object GetFormat(Type formatType)
        {
            if (formatType == typeof(ICustomFormatter))
            {
                return this;
            }

            return null;
        }

        public string Format(string format, object arg, IFormatProvider formatProvider)
        {
            if (!(arg is int))
            {
                throw new ArgumentException();
            }
            int number = (int)arg;

            if (number < 0 || number > 3999)
            {
                throw new ArgumentException("Value must be in the range 0 - 3999.");
            }

            if (number == 0)
            {
                return "N";
            }
                

            StringBuilder result = new StringBuilder();
            for (int i = 0; i < 13; i++)
            {
                while (number >= Values[i])
                {
                    number -= Values[i];
                    result.Append(Numerals[i]);
                }
            }

            return result.ToString();
        }
    }
}
