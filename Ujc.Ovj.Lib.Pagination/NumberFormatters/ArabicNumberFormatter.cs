﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ujc.Ovj.Lib.Pagination.NumberFormatters
{
    internal class ArabicNumberFormatter : IFormatProvider, ICustomFormatter
    {
        public object GetFormat(Type formatType)
        {
            if (formatType == typeof(ICustomFormatter))
            {
                return this;
            }

            return null;
        }

        public string Format(string format, object arg, IFormatProvider formatProvider)
        {
            return arg.ToString();
        }
    }
}
