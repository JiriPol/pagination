﻿using System;
using System.Text;

namespace Ujc.Ovj.Lib.Pagination.BookFormatFormatters
{
    internal class LetterBookFormatFormatter : IFormatProvider, ICustomFormatter
    {
        private static readonly char[] Letters = new[] {
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'k', 'l',
            'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'v', 'x', 'y', 'z'};

        public object GetFormat(Type formatType)
        {
            if (formatType == typeof(ICustomFormatter))
            {
                return this;
            }

            return null;
        }

        public string Format(string format, object arg, IFormatProvider formatProvider)
        {
            if (!(arg is int))
            {
                throw new ArgumentException();
            }

            int letterNumber = (int)arg;
            if (letterNumber < 0)
            {
                throw new ArgumentException($"Value must be in the range 0 - {Letters.Length}.");
            }
            if (letterNumber >= Letters.Length)
            {
                int numberOfLetters = (int)Math.Floor((decimal)letterNumber / Letters.Length) + 1;

                StringBuilder stringBuilder = new StringBuilder(numberOfLetters);
                stringBuilder.Append(Letters[letterNumber % Letters.Length].ToString().ToUpperInvariant());
                for (int i = 1; i < numberOfLetters; i++)
                {
                    stringBuilder.Append(Letters[letterNumber % Letters.Length].ToString());
                }

                return stringBuilder.ToString();
            }

            return Letters[letterNumber].ToString().ToUpperInvariant();
        }
    }
}
