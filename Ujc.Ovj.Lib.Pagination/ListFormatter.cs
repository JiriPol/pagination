﻿using System;
using Ujc.Ovj.Lib.Pagination.Abstraction;
using Ujc.Ovj.Lib.Pagination.ColumnFormatters;
using Ujc.Ovj.Lib.Pagination.NumberFormatters;
using Ujc.Ovj.Lib.Pagination.Util;

namespace Ujc.Ovj.Lib.Pagination
{
    /// <summary>
    /// Compatible formats:
    /// NUMBER_ARABIC
    /// NUMBER_ROMAN
    /// </summary>
    public class ListFormatter : IFormatProvider, ICustomFormatter
    {
        private const string GENERAL = "G";

        private const string NUMBER_ARABIC = "NUMBER_ARABIC";
        private const string NUMBER_ROMAN = "NUMBER_ROMAN";

        public object GetFormat(Type formatType)
        {
            if (formatType == typeof(ICustomFormatter))
            {
                return this;
            }

            return null;
        }

        public string Format(string format, object arg, IFormatProvider formatProvider)
        {
            if ((arg is IPage))
            {
                IPage page = (IPage)arg;
                int number = page.SequentialNumber;

                if (format == null)
                {
                    format = GENERAL;
                }

                switch (format.ToUpperInvariant())
                {
                    case GENERAL:
                        return $"{this.GetType().Name}[{page.ToString()}]";
                    case NUMBER_ARABIC:
                        return string.Format(new ArabicNumberFormatter(), "{0}", number);
                    case NUMBER_ROMAN:
                        return string.Format(new RomanNumberFormatter(), "{0}", number);
                    default:
                        throw new FormatException($"The format of '{format}' is invalid.");
                }
            }

            if (arg is IColumn)
            {
                return new ColumnFormatter().Format(format, arg, formatProvider);
            }

            return OtherFormatsHandler.HandleOtherFormats(format, arg);
        }
    }
}
