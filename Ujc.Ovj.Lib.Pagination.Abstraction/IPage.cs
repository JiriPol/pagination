﻿using System;

namespace Ujc.Ovj.Lib.Pagination.Abstraction
{
    public interface IPage
    {
        bool IsFirst { get; }
        bool IsLast { get; }
        int SequentialNumber { get; }
        IPage Next();
        IPage Previous();
    }
}
