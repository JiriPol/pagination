﻿
namespace Ujc.Ovj.Lib.Pagination.Abstraction
{
    public interface IColumn
    {
        bool IsFirst { get; }
        bool IsLast { get; }
        INumberOfColumns NumberOfColumns { get; }
        int Value { get; }
        IColumn Next();
        IColumn Previous();
    }
}
