﻿
namespace Ujc.Ovj.Lib.Pagination.Abstraction
{
    public interface INumberOfColumns
    {
        int Value { get; }
    }
}
