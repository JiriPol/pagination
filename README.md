# Pagination 

C#/.NET 4.0 library for creating page numbering of Western manuscripts, which are read by turning the pages over from right to left.

## Usage examples

```
IPage page = Page.Factory.CreateFirst();
string result1 = string.Format(new QuartoFormatter(), "{0:BOOK_LETTER}{0:NUMBER_ARABIC}{0:SIDE_RECTO_VERSO}", page);
IPage nextPage = page.Next();
string result2 = string.Format(new QuartoFormatter(), "{0:BOOK_LETTER}{0:NUMBER_ARABIC}{0:SIDE_RECTO_VERSO}", nextPage);
Console.WriteLine(result1);
Console.WriteLine(result2);

```

Outputs:

A1r
A1v

## Supported Platforms

* .NET 4.0+

## Formatters

* FolioFotmatter (fo;f)
    * NUMBER_ARABIC
    * NUMBER_ROMAN
    * SIDE_RECTO_VERSO
    * SIDE_A_B
* QuartoFormatter (4to)
    * BOOK_LETTER
    * NUMBER_ARABIC
    * NUMBER_ROMAN
    * SIDE_RECTO_VERSO
    * SIDE_A_B
* OctavoFormatter (8vo)
    * BOOK_LETTER
    * NUMBER_ARABIC
    * NUMBER_ROMAN
    * SIDE_RECTO_VERSO
    * SIDE_A_B
* DuodecimoFormatter (12mo)
    * BOOK_LETTER
    * NUMBER_ARABIC
    * NUMBER_ROMAN
    * SIDE_RECTO_VERSO
    * SIDE_A_B
* SextodecimoFormatter (16mo)
    * BOOK_LETTER
    * NUMBER_ARABIC
    * NUMBER_ROMAN
    * SIDE_RECTO_VERSO
    * SIDE_A_B

All formatters also supports column formatting using:

* COLUMN_LATIN_LETTER
* COLUMN_GREEK_LETTER
* COLUMN_ARABIC_NUMBER
* COLUMN_ROMAN_NUMBER


### Column example 
```
IPage page = Page.Factory.Create(2);
IColumn column = Column.Factory.CreateFirst(NumberOfColumns.Factory.Create(2)));
string result = string.Format(new FolioFormatter(), "{0:NUMBER_ARABIC}{0:SIDE_RECTO_VERSO}/{1:COLUMN_GREEK_LETTER}", page, column);
Console.WriteLine(result);

```

Outputs: 

1v/α

## Contributing

If you want to contribute, please feel free to grab an issue. To get your code into the repository please fork it and create a pull request with your changes.

## Authors

* **Jiri Polacek**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

